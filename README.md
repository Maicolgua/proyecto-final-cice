# Proyecto Final Cice

> Proyecto Final de Máster de Desarrollo Full-Stack en donde se realiza una aplicación completa desde su documentación, maquetado y desarollo hasta su despliegue.

## Instalación

```bash
npm install
```

## Variables de entorno

- AUTH_SECRET
- MONGO_URL
- GOOGLE_CLIENT_ID
- GOOGLE_CLIENT_SECRET
- GOOGLE_AUTH_CALLBACK

## Ejecución

```bash
npm start
```
