import React, { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { Link } from "react-router-dom";
import { getProducts, deleteProduct } from "../apiAdmin";
import {
  Button,
  TableBody,
  TableRow,
  TableCell,
  Table,
  Card,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import GoBack from "../GoBack";

function ManageProducts() {
  const [products, setProducts] = useState([]);

  const { user, token } = isAuthenticated();

  const loadProducts = () => {
    getProducts().then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setProducts(data);
      }
    });
  };

  const destroy = (productId) => {
    deleteProduct(productId, user._id, token).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        loadProducts();
      }
    });
  };

  useEffect(() => {
    loadProducts();
  }, []);

  return (
    <Layout>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Card
          style={{
            borderBottom: "5px solid #366797",
            width: "70%",
            margin: "20px 0",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              color: "#fff",
              backgroundColor: "#366797",
            }}
          >
            <h2 className="text-center">Total {products.length} productos</h2>
          </div>
          <Table>
            <TableBody>
              {products.map((p, i) => (
                <TableRow>
                  <TableCell key={i}>
                    <strong>{p.name}</strong>
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="outlined"
                      color="primary"
                      component={Link}
                      to={`/admin/product/update/${p._id}`}
                      startIcon={<EditIcon />}
                    >
                      Editar
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => destroy(p._id)}
                      startIcon={<DeleteIcon />}
                    >
                      Borrar
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div
            style={{
              padding: "20px",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <GoBack />
          </div>
        </Card>
      </div>
    </Layout>
  );
}

export default ManageProducts;
