import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { Link } from "react-router-dom";
import {
  Card,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TableHead,
  Select,
  FormControl,
  InputLabel,
  Typography,
} from "@material-ui/core";
import { listOrders, getStatusValues, updateOrderStatus } from "../apiAdmin";
import moment from "moment";
import GoBack from "../GoBack";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    marginBottom: "30px",
  },
});

function Orders() {
  const classes = useStyles();
  const [orders, setOrders] = useState([]);
  const [statusValues, setStatusValues] = useState([]);

  const { user, token } = isAuthenticated();

  const loadOrders = () => {
    listOrders(user._id, token).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setOrders(data);
      }
    });
  };

  const loadStatusValues = () => {
    getStatusValues(user._id, token).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setStatusValues(data);
      }
    });
  };

  useEffect(() => {
    loadOrders();
    loadStatusValues();
  }, []);

  const showOrdersLength = () => {
    if (orders.length > 0) {
      return (
        <Typography variant="h3">Total Pedidos: {orders.length}</Typography>
      );
    } else {
      return <hTypography variant="h3">No hay pedidos</hTypography>;
    }
  };

  const showInput = (key, value) => (
    <TableRow>
      <TableCell>{key}</TableCell>
      <TableCell>{value}</TableCell>
    </TableRow>
  );

  const handleStatusChange = (e, orderId) => {
    updateOrderStatus(user._id, token, orderId, e.target.value).then((data) => {
      if (data.error) {
        console.log("Status update failed");
      } else {
        loadOrders();
      }
    });
  };

  const showStatus = (o) => (
    <div style={{ display: "flex" }}>
      <FormControl style={{ width: "200px" }}>
        <InputLabel>Cambiar Estado</InputLabel>
        <Select onChange={(e) => handleStatusChange(e, o._id)}>
          <option style={{ cursor: "pointer" }}>Seleccione Estado</option>
          {statusValues.map((status, index) => (
            <option key={index} value={status} style={{ cursor: "pointer" }}>
              {status}
            </option>
          ))}
        </Select>
      </FormControl>
    </div>
  );

  return (
    <Layout>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div style={{ width: "70%" }}>
          <Card
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              alignItems: "center",
            }}
          >
            {showOrdersLength()}
            <GoBack />
          </Card>

          {orders.map((o, oIndex) => {
            return (
              <Card
                key={oIndex}
                style={{
                  borderBottom: "5px solid #366797",
                  borderTop: "5px solid #366797",
                  margin: "20px 0",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    color: "#fff",
                    backgroundColor: "#366797",
                  }}
                >
                  <Typography variant="h5">ID Pedido: {o._id}</Typography>
                </div>
                <div style={{ margin: "20px" }}>{showStatus(o)}</div>

                <TableContainer>
                  <Table className={classes.table}>
                    <TableHead>
                      <TableRow>
                        <TableCell>Estado</TableCell>
                        <TableCell>{o.status}</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow></TableRow>
                      <TableRow>
                        <TableCell>ID Transacción</TableCell>
                        <TableCell>{o.transaction_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Costo Total</TableCell>
                        <TableCell>${o.amount}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Cliente</TableCell>
                        <TableCell>{o.user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Fecha de pedido </TableCell>
                        <TableCell>{moment(o.createdAt).fromNow()}</TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>Dirección Delivery: {o.address}</TableCell>
                        <TableCell>{o.address}</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
                <Typography variant="h4">
                  Total de productos en el Pedido: {o.products.length}
                </Typography>

                {o.products.map((p, pIndex) => (
                  <div
                    key={pIndex}
                    style={{
                      padding: "20px",
                      borderTop: "1px solid #366797",
                    }}
                  >
                    <Table className={classes.table}>
                      <TableBody>
                        {showInput("Producto", p.name)}
                        {showInput("Precio", `$${p.price}`)}
                        {showInput("Cantidad Producto", p.count)}
                        {showInput("Id Producto", p._id)}
                      </TableBody>
                    </Table>
                  </div>
                ))}
              </Card>
            );
          })}
        </div>
      </div>
    </Layout>
  );
}

export default Orders;
