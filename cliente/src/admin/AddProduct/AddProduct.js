import React, { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { Link } from "react-router-dom";
import { createProduct, getCategories } from "../apiAdmin";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  CardActions,
  TextField,
  Button,
  Select,
  FormControl,
  Typography,
  InputLabel,
  LinearProgress,
} from "@material-ui/core";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import GoBack from "../GoBack";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const useStyles = makeStyles((theme) => ({
  input: {
    display: "none",
  },
}));

function AddProduct() {
  const [values, setValues] = useState({
    name: "",
    description: "",
    price: "",
    categories: [],
    category: "",
    quantity: "",
    photo: "",
    loading: false,
    error: "",
    createdProduct: "",
    redirectToProfile: false,
    formData: "",
  });

  const classes = useStyles();

  const { user, token } = isAuthenticated();
  const {
    name,
    description,
    price,
    categories,
    quantity,
    loading,
    error,
    createdProduct,
    redirectToProfile,
    formData,
  } = values;

  const init = () => {
    getCategories().then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error });
      } else {
        setValues({
          ...values,
          categories: data,
          formData: new FormData(),
        });
      }
    });
  };

  useEffect(() => {
    init();
  }, []);

  const handleChange = (name) => (event) => {
    const value = name === "photo" ? event.target.files[0] : event.target.value;
    formData.set(name, value);
    setValues({ ...values, [name]: value });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: "", loading: true });

    createProduct(user._id, token, formData).then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error });
        toast.error(`Error! ${error}`);
      } else {
        setValues({
          ...values,
          name: "",
          description: "",
          photo: "",
          price: "",
          quantity: "",
          loading: false,
          createdProduct: data.name,
        });

        toast.success(`${createdProduct} fue creado!`);
      }
    });
  };

  const newPostForm = () => (
    <Card
      style={{
        borderBottom: "5px solid #366797",
        margin: "20px 0",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          color: "#fff",
          backgroundColor: "#366797",
        }}
      >
        <Typography component="h4" variant="h4">
          Ingresar Producto
        </Typography>
      </div>
      <CardActions>
        <input
          accept="image/*"
          id="contained-button-file"
          className={classes.input}
          onChange={handleChange("photo")}
          multiple
          type="file"
        />
        <label htmlFor="contained-button-file">
          <Button
            startIcon={<CloudUploadIcon />}
            variant="contained"
            component="span"
            style={{ marginTop: "20px" }}
          >
            SUBIR FOTO
          </Button>
        </label>
      </CardActions>
      <CardContent>
        <FormControl style={{ width: "100%", height: "300px" }}>
          <TextField
            onChange={handleChange("name")}
            type="text"
            value={name}
            label="Nombre"
          />
          <TextField
            onChange={handleChange("description")}
            label="Descripción"
            value={description}
          />
          <TextField
            onChange={handleChange("price")}
            type="number"
            label="Precio"
            value={price}
          />
          <FormControl>
            <InputLabel id="demo-simple-select-error-label">
              Categoria
            </InputLabel>

            <Select onChange={handleChange("category")} label="Categoria">
              <option style={{ cursor: "pointer", fontFamily: "Arial" }}>
                Seleccionar Categoria
              </option>
              {categories &&
                categories.map((category, index) => (
                  <option
                    key={index}
                    value={category._id}
                    style={{ cursor: "pointer", fontFamily: "Arial" }}
                  >
                    {category.name}
                  </option>
                ))}
            </Select>
          </FormControl>
          <TextField
            onChange={handleChange("quantity")}
            type="number"
            className="form-control"
            value={quantity}
            label="Cantidad"
          ></TextField>
        </FormControl>
      </CardContent>
      <CardActions
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Button
          onClick={clickSubmit}
          startIcon={<AddCircleOutlineIcon />}
          variant="contained"
          color="primary"
        >
          Crear
        </Button>
        <GoBack />
      </CardActions>
    </Card>
  );

  const showLoading = () => loading && <LinearProgress />;

  return (
    <Layout>
      <ToastContainer />
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div style={{ width: "70%" }}>
          {showLoading()}

          {newPostForm()}
        </div>
      </div>
    </Layout>
  );
}

export default AddProduct;
