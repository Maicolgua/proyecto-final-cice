import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import Layout from "../../components/Layout";
import axios from "axios";
import { signin, authenticate, isAuthenticated } from "../../auth";
import { auth, isAuth } from "./helpers";
import {
  Button,
  TextField,
  Card,
  CardContent,
  CardActions,
  CardHeader,
  LinearProgress,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import Google from "../Google";
import Facebook from "../Facebook";
import "./Signin.css";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Signin({ history }) {
  const [values, setValues] = useState({
    email: "",
    password: "",
    error: "",
    loading: false,
    redirectToReferrer: false,
  });

  const { email, password, role, loading, error, redirectToReferrer } = values;
  const { user } = isAuthenticated();

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const informParent = (response) => {
    auth(response, () => {
      setValues({
        redirectToReferrer: true,
      });
    });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: false, loading: true });
    signin({ email, password }).then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error, loading: false });
      } else {
        authenticate(data, () => {
          setValues({
            ...values,
            redirectToReferrer: true,
          });
        });
      }
    });
  };

  const signInForm = () => (
    <Card
      style={{
        width: "400px",
        height: "400px",
      }}
    >
      <CardHeader title="LOGIN" style={{ textAlign: "center" }}></CardHeader>
      <CardContent
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <TextField
          onChange={handleChange("email")}
          type="email"
          value={email}
          label="Email"
        />

        <TextField
          onChange={handleChange("password")}
          type="password"
          label="Contraseña"
          value={password}
        />
      </CardContent>
      <CardActions
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Button onClick={clickSubmit} variant="contained" color="secondary">
          Login
        </Button>
        <div
          style={{
            margin: "10px",
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
            width: "100%",
          }}
        >
          <div style={{ padding: "20px 0" }}>
            <Google informParent={informParent} />
          </div>
          <div>
            <Facebook informParent={informParent} />
          </div>
        </div>
      </CardActions>
    </Card>
  );

  const showError = () => (
    <Alert
      style={{
        display: error ? "" : "none",
        width: "420px",
      }}
      severity="error"
    >
      {error}
    </Alert>
  );

  const showLoading = () =>
    loading && (
      <div>
        <LinearProgress />
      </div>
    );

  const redirectUser = () => {
    if (redirectToReferrer) {
      if (user && user.role === 1) {
        return <Redirect to="/admin/dashboard" />;
      } else {
        return <Redirect to="/user/dashboard" />;
      }
    }
    if (isAuthenticated()) {
      return <Redirect to="/" />;
    }
  };

  return (
    <Layout>
      <div className="loginContainer">
        {showLoading()}
        <div
          style={{ width: "100%", display: "flex", justifyContent: "center" }}
        >
          {showError()}
        </div>

        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          {signInForm()}
        </div>
        {redirectUser()}
      </div>
    </Layout>
  );
}

export default Signin;
