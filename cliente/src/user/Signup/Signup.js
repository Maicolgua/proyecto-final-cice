import React, { useState } from "react";
import { Link } from "react-router-dom";
import Layout from "../../components/Layout";
import { signup } from "../../auth";
import {
  Button,
  TextField,
  Card,
  CardContent,
  CardActions,
  CardHeader,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import "./Signup.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Signup() {
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
    error: "",
    success: false,
  });

  const { name, email, password, success, error } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: false });
    signup({ name, email, password }).then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error, success: false });
        toast.error("Error", { error });
      } else {
        setValues({
          ...values,
          name: "",
          email: "",
          password: "",
          error: "",
          success: true,
        });
        toast.success("El usuario fue creado. Ya puede ingresar a su cuenta");
      }
    });
  };

  const signUpForm = () => (
    <Card
      style={{
        width: "400px",
      }}
    >
      <CardHeader title="REGISTRO" style={{ textAlign: "center" }} />

      <CardContent
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <TextField
          onChange={handleChange("name")}
          type="text"
          label="Nombre"
          value={name}
        />
        <TextField
          onChange={handleChange("email")}
          type="email"
          label="Email"
          value={email}
        />
        <TextField
          onChange={handleChange("password")}
          type="password"
          label="Contraseña"
          value={password}
        />
      </CardContent>
      <CardActions
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Button onClick={clickSubmit} variant="contained" color="primary">
          Registrate
        </Button>
      </CardActions>
    </Card>
  );

  const showError = () => (
    <Alert
      style={{
        display: error ? "" : "none",
        width: "420px",
      }}
      severity="error"
    >
      {error}
    </Alert>
  );

  return (
    <Layout>
      <ToastContainer />
      <div className="signupContainer">
        <div
          style={{ width: "100%", display: "flex", justifyContent: "center" }}
        >
          {showError()}
        </div>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          {signUpForm()}
        </div>
      </div>
    </Layout>
  );
}

export default Signup;
