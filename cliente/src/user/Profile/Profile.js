import React, { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { Link, Redirect } from "react-router-dom";
import { read, update, updateUser } from "../apiUser";
import {
  TextField,
  Button,
  Card,
  CardContent,
  FormControl,
  CardActions,
  Typography,
} from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";

function Profile({ match }) {
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
    error: false,
    success: false,
  });

  const { token } = isAuthenticated();
  const { name, email, password, error, success } = values;

  const init = (userId) => {
    // console.log(userId);
    read(userId, token).then((data) => {
      if (data.error) {
        setValues({ ...values, error: true });
      } else {
        setValues({ ...values, name: data.name, email: data.email });
      }
    });
  };

  useEffect(() => {
    init(match.params.userId);
  }, []);

  const handleChange = (name) => (e) => {
    setValues({ ...values, error: false, [name]: e.target.value });
  };

  const clickSubmit = (e) => {
    e.preventDefault();
    update(match.params.userId, token, { name, email, password }).then(
      (data) => {
        if (data.error) {
          // console.log(data.error);
          alert(data.error);
        } else {
          updateUser(data, () => {
            setValues({
              ...values,
              name: data.name,
              email: data.email,
              success: true,
            });
          });
        }
      }
    );
  };

  const redirectUser = (success) => {
    if (success) {
      return <Redirect to="/cart" />;
    }
  };

  const profileUpdate = (name, email, password) => (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <Card
        style={{
          width: "70%",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            color: "#fff",
            backgroundColor: "#366797",
          }}
        >
          <Typography variant="h5" component="h2">
            Editar Perfil
          </Typography>
        </div>
        <CardContent>
          <FormControl style={{ width: "100%" }}>
            <TextField
              type="text"
              onChange={handleChange("name")}
              value={name}
              fullWidth
              label="Nombre"
            />

            <TextField
              type="email"
              onChange={handleChange("email")}
              value={email}
              fullWidth
              label="Email"
            />

            <TextField
              type="password"
              label="Contraseña"
              fullWidth
              onChange={handleChange("password")}
              value={password}
            />
          </FormControl>
        </CardContent>
        <CardActions>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Button
              onClick={clickSubmit}
              color="primary"
              variant="contained"
              startIcon={<SaveIcon />}
            >
              Submit
            </Button>

            <Button
              component={Link}
              startIcon={<KeyboardArrowLeftIcon />}
              to="/user/dashboard"
              variant="contained"
              color="secondary"
            >
              VOLVER
            </Button>
          </div>
        </CardActions>
      </Card>
    </div>
  );

  return (
    <Layout>
      {profileUpdate(name, email, password)}
      {redirectUser(success)}
    </Layout>
  );
}

export default Profile;
