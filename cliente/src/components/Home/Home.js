import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Layout from "../Layout";
import { getProducts } from "../apiComponentes";
import ProductCard from "../ProductCard";
import { Grid, Box } from "@material-ui/core";
import fondoPan from "../../assets/images/fondoPan.jpg";

function Home() {
  const [productsBySell, setProductsBySell] = useState([]);
  const [productsByArrival, setProductsByArrival] = useState([]);
  const [error, setError] = useState(false);

  const loadProductsBySell = () => {
    getProducts("sold").then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setProductsBySell(data);
      }
    });
  };

  const loadProductsByArrival = () => {
    getProducts("createdAt").then((data) => {
      console.log(data);
      if (data.error) {
        setError(data.error);
      } else {
        setProductsByArrival(data);
      }
    });
  };

  useEffect(() => {
    loadProductsByArrival();
    loadProductsBySell();
  }, []);

  return (
    <Layout>
      <Grid container>
        <Grid item xs={12}>
          <img
            src={fondoPan}
            alt="fondo Pan"
            width="100%"
            style={{ marginTop: "-200px" }}
          />
        </Grid>
        <Grid item xs={12} style={{ margin: "0 20px" }}>
          <h1>Nosotros</h1>
          <div style={{ fontFamily: "Arial" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
            venenatis leo quis nunc ullamcorper, vitae convallis purus faucibus.
            Phasellus varius nibh in arcu efficitur, sed rhoncus libero
            venenatis. Aliquam fringilla magna efficitur, blandit arcu vel,
            hendrerit magna. Praesent luctus gravida metus nec rutrum. Integer
            lobortis ex sed imperdiet finibus. Nulla at arcu ac tellus ornare
            placerat. Suspendisse sit amet leo tincidunt, bibendum dui ut,
            rutrum velit. Proin scelerisque lacinia urna quis bibendum.
            Vestibulum in metus imperdiet quam faucibus aliquam non ut neque.
            Quisque elit ex, egestas et facilisis id, ornare eu arcu. Vestibulum
            leo neque, lacinia at elit ac, fermentum pretium sem. Praesent ac
            orci hendrerit, dapibus tellus imperdiet, dignissim libero. In
            venenatis pulvinar rutrum. Sed hendrerit facilisis ullamcorper.
            Maecenas feugiat id est at faucibus. Duis elementum, tellus vitae
            mollis dapibus, metus tortor commodo libero, sed condimentum sem
            nisi aliquam justo. Vivamus vel tortor pharetra, semper ante in,
            feugiat lorem. Sed in metus a urna venenatis accumsan. Mauris
            efficitur laoreet pharetra. Integer diam ante, dignissim in eleifend
            sit amet, vestibulum sit amet urna. Donec sed elit finibus, mattis
            est at, gravida mi. Sed posuere urna in orci sodales tempus.
            Maecenas dictum velit vel odio pretium ultrices. Quisque semper eros
            elit, vel sagittis ante egestas at. Etiam nunc nisi, vehicula id
            consectetur at, pulvinar non quam. Sed volutpat eu urna eget semper.
            Quisque tincidunt lacus sed tortor iaculis, at hendrerit mi rutrum.
          </div>
        </Grid>

        <Grid item xs={12} style={{ margin: "30px 0" }}>
          <h2 style={{ margin: "10px 20px" }}>Recien Horneados</h2>

          <div className="grid-container">
            {productsByArrival.map((product, i) => (
              <div key={i} className="grid-item">
                <ProductCard product={product} />
              </div>
            ))}
          </div>
        </Grid>

        <Grid item xs={12}>
          <h2 style={{ margin: "10px 20px" }}>Se venden como pan caliente!</h2>
          <div className="grid-container">
            {productsBySell.map((product, i) => (
              <div key={i} className="grid-item">
                <ProductCard product={product} />
              </div>
            ))}
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
}

export default Home;
