import React from "react";
import { Link, withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { signout, isAuthenticated } from "../../auth";
import { AppBar, Toolbar, Button, IconButton } from "@material-ui/core";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { itemTotal } from "../cartHelpers";
import logo from "../../assets/logo.svg";
import "./Menu.css";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const isActive = (history, path) => {
  if (history.location.pathname === path) {
    return { color: "#BE3200" };
  } else {
    return { color: "#000" };
  }
};

function Menu({ history }) {
  const classes = useStyles();
  const { user } = isAuthenticated();

  return (
    <div className={classes.root}>
      <AppBar position="fixed" style={{ backgroundColor: "#fff" }}>
        <Toolbar
          style={{
            display: "flex",

            justifyContent: "space-between",
          }}
        >
          <div className="navigation">
            <Button
              component={Link}
              color="inherit"
              style={isActive(history, "/")}
              to="/"
            >
              Home
            </Button>

            <Button
              component={Link}
              color="inherit"
              style={isActive(history, "/shop")}
              to="/shop"
            >
              Productos
            </Button>
            <Button
              component={Link}
              color="inherit"
              style={isActive(history, "/contact")}
              to="/contact"
            >
              Contacto
            </Button>
          </div>

          <Link className="logo-container" to="/">
            <img src={logo} width="100px" alt="logo" />
          </Link>

          <div>
            <div className="user-container">
              <IconButton
                component={Link}
                color="inherit"
                style={isActive(history, "/cart")}
                to="/cart"
              >
                <ShoppingCartIcon />
                <sup>
                  <small>{itemTotal()}</small>
                </sup>
              </IconButton>
              {isAuthenticated() && isAuthenticated().user.role === 0 && (
                <Button
                  component={Link}
                  style={isActive(history, "/user/dashboard")}
                  to="/user/dashboard"
                >
                  {user.name}
                </Button>
              )}
              {isAuthenticated() && isAuthenticated().user.role === 1 && (
                <Button
                  component={Link}
                  style={isActive(history, "/admin/dashboard")}
                  to="/admin/dashboard"
                >
                  {user.name}
                </Button>
              )}
              {!isAuthenticated() && (
                <>
                  <Button
                    component={Link}
                    style={isActive(history, "/signin")}
                    to="/signin"
                  >
                    Login
                  </Button>

                  <Button
                    component={Link}
                    style={isActive(history, "/signup")}
                    to="/signup"
                  >
                    Registrate
                  </Button>
                </>
              )}
              {isAuthenticated() && (
                <IconButton
                  style={{ cursor: "pointer", color: "#000" }}
                  onClick={() =>
                    signout(() => {
                      history.push("/");
                    })
                  }
                >
                  <ExitToAppIcon />
                </IconButton>
              )}
            </div>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withRouter(Menu);
