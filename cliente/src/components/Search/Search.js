import React, { useState, useEffect } from "react";
import { getCategories, list } from "../apiComponentes";
import ProductCard from "../ProductCard";
import SearchIcon from "@material-ui/icons/Search";
import {
  TextField,
  Select,
  FormControl,
  Button,
  InputLabel,
  Card,
} from "@material-ui/core";

function Search() {
  const [data, setData] = useState({
    categories: [],
    category: "",
    search: "",
    results: [],
    searched: false,
  });

  const { categories, category, search, results, searched } = data;

  const loadCategories = () => {
    getCategories().then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setData({ ...data, categories: data });
      }
    });
  };

  useEffect(() => {
    loadCategories();
  }, []);

  const searchData = () => {
    if (search) {
      list({ search: search || undefined, category: category }).then(
        (response) => {
          if (response.error) {
            console.log(response.error);
          } else {
            setData({ ...data, results: response, searched: true });
          }
        }
      );
    }
  };

  const searchSubmit = (e) => {
    e.preventDefault();
    searchData();
  };

  const handleChange = (name) => (event) => {
    setData({ ...data, [name]: event.target.value, searched: false });
  };

  const searchMessage = (searched, results) => {
    if (searched && results.length > 0) {
      return `Se econtraron ${results.length} productos`;
    }
    if (searched && results.length < 1) {
      return `No se encontaron productos`;
    }
  };

  const searchedProducts = (results = []) => {
    return (
      <Card>
        <h2 style={{ margin: "20px 0" }}>{searchMessage(searched, results)}</h2>

        <div style={{ display: "flex", margin: "0 20px" }}>
          {results.map((product, i) => (
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "center",
              }}
            >
              <ProductCard key={i} product={product} />
            </div>
          ))}
        </div>
      </Card>
    );
  };

  const searchForm = () => (
    <div style={{ display: "flex", height: "40px", alignItems: "center" }}>
      <TextField
        variant="outlined"
        style={{ backgroundColor: "#fff" }}
        size="small"
        onChange={handleChange("search")}
        label="Buscar Producto..."
      />

      <Button
        onClick={searchSubmit}
        variant="contained"
        color="primary"
        style={{ fontWeight: "bold", marginLeft: "10px" }}
        startIcon={<SearchIcon />}
      >
        Buscar
      </Button>
    </div>
  );

  return (
    <div style={{ margin: "20px" }}>
      <div>{searchForm()}</div>
      <div display="flex">{searchedProducts(results)}</div>
    </div>
  );
}

export default Search;
