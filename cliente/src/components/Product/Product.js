import React, { useState, useEffect } from "react";
import Layout from "../Layout";
import { read, listRelated } from "../apiComponentes";
import ProductCard from "../ProductCard";
import { Card } from "@material-ui/core";

function Product(props) {
  const [product, setProduct] = useState({});
  const [relatedProduct, setRelatedProduct] = useState([]);
  const [error, setError] = useState(false);

  const loadSingleProduct = (productId) => {
    read(productId).then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setProduct(data);

        listRelated(data._id).then((data) => {
          if (data.error) {
            setError(data.error);
          } else {
            setRelatedProduct(data);
          }
        });
      }
    });
  };

  useEffect(() => {
    const productId = props.match.params.productId;
    loadSingleProduct(productId);
  }, [props]);

  return (
    <Layout>
      <div
        style={{
          display: "flex",

          justifyContent: "space-around",
        }}
      >
        <Card
          style={{
            width: "60%",
            display: "flex",
            borderBottom: "5px solid #366797 ",
            borderTop: "5px solid #366797 ",
            marginBottom: "20px",
          }}
        >
          <div style={{ marginTop: "20px", display: "flex" }}>
            <div>
              {product && product.description && (
                <ProductCard product={product} showViewProductButton={false} />
              )}
            </div>
            <div style={{ marginLeft: "30px" }}>
              <h2>{product && product.name}</h2>

              <h5>
                {product &&
                  product.description &&
                  product.description.substring(0, 100)}
              </h5>
            </div>
          </div>
        </Card>
        <div>
          <h4>Productos Recomendados</h4>
          {relatedProduct.map((p, i) => (
            <div key={i}>
              <ProductCard product={p} />
            </div>
          ))}
        </div>
      </div>
    </Layout>
  );
}

export default Product;
