import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout";
import { getCart } from "../cartHelpers";
import ProductCard from "../ProductCard";
import Checkout from "../CheckOut";
import { Card, Typography } from "@material-ui/core";

function Cart() {
  const [items, setItems] = useState([]);
  const [run, setRun] = useState(false);

  useEffect(() => {
    setItems(getCart());
  }, [run]);

  const showItems = (items) => {
    return (
      <Card
        style={{ borderBottom: "5px solid #366797 ", marginBottom: "20px" }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            color: "#fff",
            backgroundColor: "#366797",
          }}
        >
          <Typography component="h4" variant="h4">
            Tu carrito tiene {`${items.length}`} items
          </Typography>
        </div>

        {items.map((product, i) => (
          <div style={{ margin: "30px" }}>
            <ProductCard
              key={i}
              product={product}
              showAddToCartButton={false}
              cartUpdate={true}
              showRemoveProductButton={true}
              setRun={setRun}
              run={run}
            />
          </div>
        ))}
      </Card>
    );
  };

  const noItemsMessage = () => (
    <h2 className="text">
      Tu carrito esta vacío. <br /> <Link to="/shop">Continuar comprando</Link>
    </h2>
  );

  return (
    <Layout>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <div>{items.length > 0 ? showItems(items) : noItemsMessage()}</div>

        <div>
          <Checkout products={items} setRun={setRun} run={run} />
        </div>
      </div>
    </Layout>
  );
}

export default Cart;
