import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link, Redirect } from "react-router-dom";
import ShowImage from "../ShowImage";
import { addItem, updateItem, removeItem } from "../cartHelpers";
import { addLike, removeLike } from "../likeHelpers";
import {
  Card,
  CardContent,
  CardMedia,
  CardActions,
  IconButton,
  Typography,
  Chip,
  Button,
  TextField,
  FormControl,
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import RemoveShoppingCartIcon from "@material-ui/icons/RemoveShoppingCart";
import "./ProductCard.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "345px",
    borderBottom: "3px solid #BE3200 ",
    marginBottom: "20px",
  },
}));

function ProductCard({
  product,
  showViewProductButton = true,
  showAddToCartButton = true,
  cartUpdate = false,
  showRemoveProductButton = false,
  setRun = (f) => f,
  run = undefined,
}) {
  const [redirect, setRedirect] = useState(false);
  const initialState = () => window.localStorage.getItem("color") || false;
  const [like, setLike] = useState(initialState);
  const [count, setCount] = useState(product.count);
  const classes = useStyles();

  const showViewButton = (showViewProductButton) => {
    return (
      showViewProductButton && (
        <Button
          variant="outlined"
          component={Link}
          color="primary"
          to={`/product/${product._id}`}
        >
          Ver Producto
        </Button>
      )
    );
  };
  const addToCart = () => {
    addItem(product, setRedirect(true));
  };

  const handleLike = () => {
    addLike(product, setLike(!like));
  };

  const handleUnlike = () => {
    if (product._id) {
      removeLike(product._id, setLike(!like));
    } else {
      addLike(product);
    }
  };

  useEffect(() => {
    window.localStorage.setItem("color", colorValue);
  }, [like]);

  const shouldRedirect = (redirect) => {
    if (redirect) {
      return <Redirect to="/cart" />;
    }
  };

  const showAddToCartBtn = (showAddToCartButton) => {
    return (
      showAddToCartButton && (
        <IconButton aria-label="add to cart" onClick={addToCart}>
          <ShoppingCartIcon />
        </IconButton>
      )
    );
  };

  const showStock = (quantity) => {
    return quantity > 0 ? (
      <Chip label="En Stock" size="small" color="primary" />
    ) : (
      <Chip label="Agotado" size="small" color="secondary" />
    );
  };

  const handleChange = (productId) => (event) => {
    setRun(!run);
    setCount(event.target.value < 1 ? 1 : event.target.value);
    if (event.target.value >= 1) {
      updateItem(productId, event.target.value);
    }
  };

  const showCartUpdateOptions = (cartUpdate) => {
    return (
      cartUpdate && (
        <FormControl style={{ marginTop: "20px" }}>
          <TextField
            type="number"
            variant="outlined"
            value={count}
            size="small"
            label="Cantidad"
            onChange={handleChange(product._id)}
          />
        </FormControl>
      )
    );
  };
  const showRemoveButton = (showRemoveProductButton) => {
    return (
      showRemoveProductButton && (
        <IconButton
          onClick={() => {
            removeItem(product._id);
            setRun(!run);
          }}
          variant="outlined"
          color="secondary"
        >
          <RemoveShoppingCartIcon />
        </IconButton>
      )
    );
  };
  const colorValue = like ? "#BE3200" : "";
  return (
    <Card className={classes.root}>
      <div
        className="title"
        style={{
          backgroundColor: "#BE3200",
          color: "#fff",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {product.name}
      </div>

      <CardContent>
        {shouldRedirect(redirect)}
        <CardMedia>
          <ShowImage item={product} url="product" />
        </CardMedia>

        <Typography>Precio: </Typography>
        <Typography>$ {product.price}</Typography>
        <Typography>{product.category && product.category.name}</Typography>
        <div style={{ paddingTop: "10px" }}>{showStock(product.quantity)}</div>

        {showCartUpdateOptions(cartUpdate)}
      </CardContent>
      <CardActions style={{ display: "flex", justifyContent: "center" }}>
        <IconButton aria-label="add to favorites" onClick={handleLike}>
          <FavoriteIcon style={{ color: colorValue }} />
        </IconButton>
        <IconButton aria-label="add to favorites" onClick={handleUnlike}>
          <FavoriteIcon />
        </IconButton>
        {showViewButton(showViewProductButton)}

        {showAddToCartBtn(showAddToCartButton)}

        {showRemoveButton(showRemoveProductButton)}
      </CardActions>
    </Card>
  );
}

export default ProductCard;
