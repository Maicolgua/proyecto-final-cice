import React from "react";
import { API } from "../../config";

function ShowImage({ item, url }) {
  return (
    <div>
      <img
        src={`${API}/${url}/photo/${item._id}`}
        alt={item.name}
        style={{ maxHeight: "200px", width: "100%", marginBottom: "10px" }}
      />
    </div>
  );
}
export default ShowImage;
