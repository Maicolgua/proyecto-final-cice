import React, { useState } from "react";
import Layout from "../../components/Layout";
import { contact } from "../../auth";
import {
  Button,
  TextField,
  Card,
  CardContent,
  CardActions,
  CardHeader,
} from "@material-ui/core";
import "./Contact.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Contact() {
  const [values, setValues] = useState({
    name: "",
    email: "",
    subject: "",
    message: "",
    error: "",
    success: false,
  });

  const { name, email, subject, message, success, error } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: false });
    contact({ name, email, subject, message }).then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error, success: false });

        toast.error("Error, intentar nuevamente");
      } else {
        setValues({
          ...values,
          name: "",
          email: "",
          subject: "",
          message: "",
          error: "",
          success: true,
        });

        toast.success("El mensaje fue enviado");
      }
    });
  };

  const ContactForm = () => (
    <Card
      style={{
        width: "400px",
      }}
    >
      <CardHeader
        title="Contactate con nosotros"
        style={{ textAlign: "center" }}
      />

      <CardContent
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <TextField
          onChange={handleChange("name")}
          type="text"
          label="Nombre"
          value={name}
        />
        <TextField
          onChange={handleChange("email")}
          type="email"
          label="Email"
          value={email}
        />
        <TextField
          onChange={handleChange("subject")}
          type="subject"
          label="Asunto"
          value={subject}
        />
        <TextField
          onChange={handleChange("message")}
          type="message"
          label="Mensaje"
          value={message}
        />
      </CardContent>
      <CardActions
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Button onClick={clickSubmit} variant="contained" color="primary">
          Enviar
        </Button>
      </CardActions>
    </Card>
  );

  return (
    <Layout>
      <ToastContainer />
      <div className="contactContainer">
        <div
          style={{ width: "100%", display: "flex", justifyContent: "center" }}
        ></div>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          {ContactForm()}
        </div>
      </div>
    </Layout>
  );
}

export default Contact;
