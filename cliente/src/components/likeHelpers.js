export const addLike = (item = [], count = 0, color = "", next = (f) => f) => {
  let like = [];
  if (typeof window !== "undefined") {
    if (localStorage.getItem("like")) {
      like = JSON.parse(localStorage.getItem("like"));
    }
    like.push({
      ...item,
      color: "green",
      count: 1,
    });

    like = Array.from(new Set(like.map((p) => p._id))).map((id) => {
      return like.find((p) => p._id === id);
    });

    localStorage.setItem("like", JSON.stringify(like));
    next();
  }
};

export const getLike = () => {
  if (typeof window !== "undefined") {
    if (localStorage.getItem("like")) {
      return JSON.parse(localStorage.getItem("like"));
    }
  }
  return [];
};

export const removeLike = (productId) => {
  let like = [];
  if (typeof window !== "undefined") {
    if (localStorage.getItem("like")) {
      like = JSON.parse(localStorage.getItem("like"));
    }

    like.map((product, i) => {
      if (product._id === productId) {
        like.splice(i, 1);
      }
    });

    localStorage.setItem("like", JSON.stringify(like));
  }
  return like;
};
