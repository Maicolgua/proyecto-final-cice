import React from "react";
import Menu from "../Menu";

import "../../styles.css";

function Layout({ children }) {
  return (
    <div>
      <Menu />

      <div style={{ marginTop: "200px" }}>{children}</div>
    </div>
  );
}

export default Layout;
