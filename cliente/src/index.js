import React from "react";
import ReactDOM from "react-dom";
import Routes from "./Routes";
import App from "./App";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  //shadows: ["none"],
  palette: {
    primary: {
      light: "#d6e0ea",
      main: "#366797",
      dark: "#1b334b",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ebc1b2",
      main: "#BE3200",
      dark: "#5f1900",
      contrastText: "#fff",
    },
  },
  typography: {
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
});

ReactDOM.render(
  <React.StrictMode>
    <MuiThemeProvider theme={theme}>
      <Routes />
    </MuiThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
