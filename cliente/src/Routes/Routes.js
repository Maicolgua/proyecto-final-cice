import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Signup from "../user/Signup";
import Signin from "../user/Signin";
import Home from "../components/Home";
import Contact from "../components/Contact";
import PrivateRoute from "../auth/PrivateRoute";
import AdminRoute from "../auth/AdminRoute";
import UserDashboard from "../user/UserDashboard";
import AdminDashboard from "../user/AdminDashboard";
import AddCategory from "../admin/AddCategory";
import AddProduct from "../admin/AddProduct";
import Shop from "../components/Shop";
import Product from "../components/Product";
import Cart from "../components/Cart";
import Orders from "../admin/Orders";
import Profile from "../user/Profile";
import ManageProducts from "../admin/ManageProducts";
import UpdateProduct from "../admin/UpdateProduct";

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/shop" exact component={Shop} />
        <Route path="/contact" exact component={Contact} />
        <Route path="/signin" exact component={Signin} />
        <Route path="/signup" exact component={Signup} />
        <PrivateRoute path="/user/dashboard" exact component={UserDashboard} />
        <AdminRoute path="/admin/dashboard" exact component={AdminDashboard} />
        <AdminRoute path="/admin/dashboard" exact component={AdminDashboard} />
        <AdminRoute path="/create/category" exact component={AddCategory} />
        <AdminRoute path="/create/product" exact component={AddProduct} />
        <Route path="/product/:productId" exact component={Product} />
        <AdminRoute path="/admin/orders" exact component={Orders} />
        <Route path="/cart" exact component={Cart} />
        <PrivateRoute path="/profile/:userId" exact component={Profile} />
        <AdminRoute path="/admin/products" exact component={ManageProducts} />
        <AdminRoute
          path="/admin/product/update/:productId"
          exact
          component={UpdateProduct}
        />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
